INSERT INTO patients (name, "phoneNumber", birthday)
VALUES ('Gozhan Alexandr', '+79618536727', '2003-12-12'),
       ('Gozhan Igor', '+88005553535', '1980-10-28');

INSERT INTO users_patients (user_id, patient_id)
VALUES (2, 1),
       (3, 2);
