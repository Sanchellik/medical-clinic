INSERT INTO users (name, username, password)
VALUES ('Dr. Jane Doe', 'JaneDoe',
        '$2a$10$v6QdKgp.JNAidmbwTvXAyeDtIWsHhjEHsA8vkr3BLIpcDGGEmx1Ae');

INSERT INTO users_roles (user_id, role)
VALUES (4, 'ROLE_DOCTOR');

INSERT INTO users_doctors (user_id, doctor_id)
VALUES (4, 2);
