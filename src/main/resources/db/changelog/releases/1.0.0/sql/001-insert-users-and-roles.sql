INSERT INTO users (name, username, password)
VALUES ('admin', 'admin',
        '$2a$10$wWKfKC3sDial7VUCFmlowuqIx4jr7Y4/hkWnuabyc/n2RaVLmXCuq'),
       ('Gozhan Alexandr', 'sanchellik',
        '$2a$10$v6QdKgp.JNAidmbwTvXAyeDtIWsHhjEHsA8vkr3BLIpcDGGEmx1Ae'),
       ('Gozhan Igor', 'igorgozhan',
        '$2a$10$v6QdKgp.JNAidmbwTvXAyeDtIWsHhjEHsA8vkr3BLIpcDGGEmx1Ae');

INSERT INTO users_roles (user_id, role)
VALUES (1, 'ROLE_REGISTRAR'),
       (2, 'ROLE_PATIENT'),
       (3, 'ROLE_PATIENT');
