INSERT INTO doctors (id, name, specialization_id, first_work_day)
VALUES (1, 'Dr. John Doe', 1, '2011-04-11');

INSERT INTO doctors (id, name, specialization_id, first_work_day)
VALUES
-- для Кардиологии
(2, 'Dr. Jane Doe', 2, '2022-02-15'),
(3, 'Dr. Sam Smith', 2, '2011-04-11'),
(4, 'Dr. Emily Davis', 2, '2011-04-11'),

-- для Дерматологии
(5, 'Dr. David Wilson', 3, '2001-10-05'),
(6, 'Dr. Sophia Johnson', 3, '2011-04-11'),
(7, 'Dr. Michael Taylor', 3, '2019-05-31'),

-- для Гастроэнтерологии
(8, 'Dr. Olivia Miller', 4, '1988-03-22'),
(9, 'Dr. James Brown', 4, '2007-03-17'),
(10, 'Dr. Amelia Anderson', 4, '2022-08-10'),

-- для Гинекологии
(11, 'Dr. Lucas Thomas', 5, '2022-08-11'),
(12, 'Dr. Ava Harris', 5, '2007-03-17'),
(13, 'Dr. William Clark', 5, '1996-05-02'),

-- для Ортопедии
(14, 'Dr. Harper Lewis', 6, '2022-08-14'),
(15, 'Dr. Benjamin Walker', 6, '2007-03-17'),
(16, 'Dr. Emma Hall', 6, '2015-05-06');

-- для Офтальмологии
INSERT INTO doctors (id, name, specialization_id, first_work_day)
VALUES (17, 'Dr. Ethan Young', 7, '2022-08-17'),
       (18, 'Dr. Mia King', 7, '2007-03-17'),
       (19, 'Dr. Noah Wright', 7, '2015-05-06'),

-- для Оториноларингологии
       (20, 'Dr. Aria Green', 8, '2022-08-20'),
       (21, 'Dr. Liam Baker', 8, '2007-03-17'),
       (22, 'Dr. Isabella Adams', 8, '2015-05-06'),

-- для Неврологии
       (23, 'Dr. Oliver Nelson', 9, '2022-08-23'),
       (24, 'Dr. Charlotte Hill', 9, '2007-03-17'),
       (25, 'Dr. Elijah Carter', 9, '2015-05-06'),

-- для Психиатрии
       (26, 'Dr. Scarlett Mitchell', 10, '2022-08-26'),
       (27, 'Dr. Lucas Roberts', 10, '1996-05-02'),
       (28, 'Dr. Sofia Cook', 10, '2015-05-06'),

-- для Урологии
       (29, 'Dr. Jackson Brooks', 11, '2022-08-29'),
       (30, 'Dr. Aria Foster', 11, '2007-03-17'),
       (31, 'Dr. Oliver Reed', 11, '2015-05-06'),

-- для Хирургии
       (32, 'Dr. Harper Powell', 12, '2022-09-01'),
       (33, 'Dr. Emma Patterson', 12, '2007-03-17'),
       (34, 'Dr. Benjamin Morris', 12, '2015-05-06'),

-- для Пульмонологии
       (35, 'Dr. Ava Cook', 13, '2015-05-06'),
       (36, 'Dr. Noah Cooper', 13, '2007-03-17'),
       (37, 'Dr. Isabella Stewart', 13, '2022-09-06'),

-- для Ревматологии
       (38, 'Dr. Amelia Ross', 14, '2015-05-06'),
       (39, 'Dr. Elijah Bennett', 14, '2007-03-17'),
       (40, 'Dr. Charlotte Foster', 14, '2022-09-09'),

-- для Эндокринологии
       (41, 'Dr. Oliver Griffin', 15, '2015-05-06'),
       (42, 'Dr. Mia Hayes', 15, '2007-03-17'),
       (43, 'Dr. Ethan Parker', 15, '2022-09-12');
