package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.exception.DoctorNotFoundException;
import ru.gozhan.medicalclinic.exception.PatientNotFoundException;
import ru.gozhan.medicalclinic.persistence.repository.UserPatientAndDoctorRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.OptionalInt;

@Repository
@RequiredArgsConstructor
public class UserPatientAndDoctorRepositoryImpl implements UserPatientAndDoctorRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Integer findPatientIdByUserId(int userId) {
        String query = """
                SELECT patient_id
                FROM users_patients
                WHERE user_id = :userId
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId);

        try {
            return jdbcTemplate.queryForObject(
                    query,
                    sqlParameterSource,
                    new PatientIntegerRowMapper()
            );
        } catch (EmptyResultDataAccessException e) {
            throw new PatientNotFoundException("Пациент с таким id не найден");
        }
    }

    @Override
    public Integer findDoctorIdByUserId(int userId) {
        String query = """
                SELECT doctor_id
                FROM users_doctors
                WHERE user_id = :userId
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId);

        try {
            return jdbcTemplate.queryForObject(
                    query,
                    sqlParameterSource,
                    new DoctorIntegerRowMapper()
            );
        } catch (EmptyResultDataAccessException e) {
            throw new DoctorNotFoundException("Врач с таким id не найден");
        }
    }

    private static class PatientIntegerRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getInt("patient_id");
        }

    }

    private static class DoctorIntegerRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getInt("doctor_id");
        }

    }

}
