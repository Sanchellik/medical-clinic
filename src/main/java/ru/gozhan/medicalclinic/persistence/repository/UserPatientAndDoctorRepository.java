package ru.gozhan.medicalclinic.persistence.repository;

public interface UserPatientAndDoctorRepository {
    Integer findPatientIdByUserId(int userId);
    Integer findDoctorIdByUserId(int userId);
}
