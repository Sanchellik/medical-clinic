package ru.gozhan.medicalclinic.persistence.repository;

import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

public interface SpecializationRepository {

    List<SpecializationEntity> findAll();

}
