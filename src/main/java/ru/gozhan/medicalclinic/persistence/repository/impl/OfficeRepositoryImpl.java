package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.persistence.repository.OfficeRepository;

import java.sql.Timestamp;

@Repository
@RequiredArgsConstructor
@Slf4j
public class OfficeRepositoryImpl implements OfficeRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public int findAvailableByTimestampAndProcedureId(Timestamp timestamp, int procedureId) {
        String query = """
                
                """;

        return 0;
    }

}
