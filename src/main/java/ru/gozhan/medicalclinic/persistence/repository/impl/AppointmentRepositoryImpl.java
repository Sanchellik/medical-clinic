package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentDto;
import ru.gozhan.medicalclinic.persistence.entity.AppointmentEntity;
import ru.gozhan.medicalclinic.persistence.entity.PassedAppointmentEntity;
import ru.gozhan.medicalclinic.persistence.repository.AppointmentRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
@Slf4j
public class AppointmentRepositoryImpl implements AppointmentRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public int save(AppointmentEntity appointmentEntity) {
        log.info("Сохранение в бд записи на приём {}", appointmentEntity);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO appointments (patient_id, doctor_id, procedure_id, office_id, date_and_time, timestamp)
                VALUES (:patientId, :doctorId, :procedureId, :officeId, :dateAndTime, :timestamp)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("patientId", appointmentEntity.getPatientId())
                .addValue("doctorId", appointmentEntity.getDoctorId())
                .addValue("procedureId", appointmentEntity.getProcedureId())
                .addValue("officeId", appointmentEntity.getOfficeId())
                .addValue("dateAndTime", appointmentEntity.getDateAndTime())
                .addValue("timestamp", appointmentEntity.getCreatedTimestamp());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public List<AppointmentDto> findInfoByPatientId(int patientId) {
        log.info("Ищем заявки для пациента с id = {}", patientId);

        String query = """
                SELECT
                    a.id AS id,
                    pa.name AS patient_name,
                    d.name AS doctor_name,
                    pr.name AS procedure_name,
                    pr.cost AS procedure_cost,
                    pr.duration AS procedure_duration,
                    o.number AS office_number,
                    a.date_and_time AS date_time
                FROM appointments a
                    JOIN public.patients pa on pa.id = a.patient_id
                    JOIN public.doctors d ON d.id = a.doctor_id
                    JOIN public.procedures pr on pr.id = a.procedure_id
                    JOIN public.offices o on o.id = a.office_id
                    LEFT JOIN passed_appointments ON a.id = passed_appointments.appointment_id
                WHERE passed_appointments.appointment_id IS NULL
                    AND a.patient_id = :patientId
                ORDER BY a.date_and_time
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("patientId", patientId);

        return jdbcTemplate.query(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(AppointmentDto.class)
        );
    }

    @Override
    public List<AppointmentDto> findInfoByDoctorId(int doctorId) {
        log.info("Ищем заявки для врача с id = {}", doctorId);

        String query = """
                SELECT
                    a.id AS id,
                    pa.name AS patient_name,
                    d.name AS doctor_name,
                    pr.name AS procedure_name,
                    pr.cost AS procedure_cost,
                    pr.duration AS procedure_duration,
                    o.number AS office_number,
                    a.date_and_time AS date_time
                FROM appointments a
                    JOIN public.patients pa on pa.id = a.patient_id
                    JOIN public.doctors d ON d.id = a.doctor_id
                    JOIN public.procedures pr on pr.id = a.procedure_id
                    JOIN public.offices o on o.id = a.office_id
                    LEFT JOIN passed_appointments ON a.id = passed_appointments.appointment_id
                WHERE passed_appointments.appointment_id IS NULL
                    AND a.doctor_id = :doctorId
                ORDER BY a.date_and_time
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("doctorId", doctorId);

        return jdbcTemplate.query(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(AppointmentDto.class)
        );
    }

    @Override
    public Integer findDoctorIdById(int appointmentId) {
        String query = """
                SELECT doctor_id
                FROM appointments
                WHERE id = :id
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", appointmentId);

        return jdbcTemplate.queryForObject(
                query,
                sqlParameterSource,
                new DoctorIdRowMapper()
        );
    }

    @Override
    public Integer savePassedAppointment(PassedAppointmentEntity passedAppointmentEntity) {
        log.info("Сохранение в бд записи о проведённом приёме {}", passedAppointmentEntity);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO passed_appointments (appointment_id, conclusion, timestamp)
                VALUES (:appointmentId, :conclusion, :timestamp)
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("appointmentId", passedAppointmentEntity.getAppointmentId())
                .addValue("conclusion", passedAppointmentEntity.getConclusion())
                .addValue("timestamp", passedAppointmentEntity.getTimestamp());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public List<PassedAppointmentDto> findPassedInfoByPatientId(int patientId) {
        log.info("Ищем заявки из архива для пациента с id = {}", patientId);

        String query = """
                SELECT
                    pa.id AS passed_appointment_id,
                    pa.appointment_id AS appointment_id,
                    pat.name AS patient_name,
                    d.name AS doctor_name,
                    pr.name AS procedure_name,
                    pr.cost AS procedure_cost,
                    a.date_and_time AS date_time,
                    pa.conclusion AS conclusion
                FROM passed_appointments pa
                    JOIN appointments a ON a.id = pa.appointment_id
                    JOIN public.patients pat on pat.id = a.patient_id
                    JOIN public.doctors d ON d.id = a.doctor_id
                    JOIN public.procedures pr on pr.id = a.procedure_id
                WHERE pat.id = :patientId
                ORDER BY a.date_and_time
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("patientId", patientId);

        return jdbcTemplate.query(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(PassedAppointmentDto.class)
        );
    }

    @Override
    public List<PassedAppointmentDto> findPassedInfoByDoctorId(int doctorId) {
        log.info("Ищем заявки из архива для врача с id = {}", doctorId);

        String query = """
                SELECT
                    pa.id AS passed_appointment_id,
                    pa.appointment_id AS appointment_id,
                    pat.name AS patient_name,
                    d.name AS doctor_name,
                    pr.name AS procedure_name,
                    pr.cost AS procedure_cost,
                    a.date_and_time AS date_time,
                    pa.conclusion AS conclusion
                FROM passed_appointments pa
                    JOIN appointments a ON a.id = pa.appointment_id
                    JOIN public.patients pat on pat.id = a.patient_id
                    JOIN public.doctors d ON d.id = a.doctor_id
                    JOIN public.procedures pr on pr.id = a.procedure_id
                WHERE d.id = :doctorId
                ORDER BY a.date_and_time
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("doctorId", doctorId);

        return jdbcTemplate.query(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(PassedAppointmentDto.class)
        );
    }

    private static class DoctorIdRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getInt("doctor_id");
        }

    }

}
