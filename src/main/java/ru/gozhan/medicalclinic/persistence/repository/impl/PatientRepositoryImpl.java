package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.persistence.entity.PatientEntity;
import ru.gozhan.medicalclinic.persistence.repository.PatientRepository;

import java.util.Objects;

@Repository
@RequiredArgsConstructor
@Slf4j
public class PatientRepositoryImpl implements PatientRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void update(PatientEntity patientEntity) {
        log.info("Обновление данных о пациенте {}", patientEntity);

        String query = """
                UPDATE patients
                SET name = :name,
                    "phoneNumber" = :phoneNumber,
                    birthday = :birthday
                WHERE id = :id
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", patientEntity.getId())
                .addValue("name", patientEntity.getName())
                .addValue("phoneNumber", patientEntity.getPhoneNumber())
                .addValue("birthday", patientEntity.getBirthday());

        jdbcTemplate.update(
                query,
                sqlParameterSource
        );
    }

    @Override
    public PatientEntity findById(int patientId) {
        String query = """
                SELECT *
                FROM patients
                WHERE id = :id
                """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", patientId);

        return jdbcTemplate.queryForObject(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(PatientEntity.class)
        );
    }

    @Override
    public int save(PatientEntity patientEntity) {
        log.info("Сохранение в бд пациента {}", patientEntity);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO patients (name, "phoneNumber", birthday)
                VALUES (:name, :phoneNumber, :birthday)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", patientEntity.getName())
                .addValue("phoneNumber", patientEntity.getPhoneNumber())
                .addValue("birthday", patientEntity.getBirthday());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public void saveUsersPatients(Long userId, int patientId) {
        log.info("Сохранение в таблице users_patients ({}, {})", userId, patientId);

        String query = """
                INSERT INTO users_patients (user_id, patient_id)
                VALUES (:userId, :patientId)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("patientId", patientId);

        jdbcTemplate.update(
                query,
                sqlParameterSource
        );
    }

}
