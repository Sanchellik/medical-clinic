package ru.gozhan.medicalclinic.persistence.repository;

import java.sql.Timestamp;

public interface OfficeRepository {
    int findAvailableByTimestampAndProcedureId(Timestamp timestamp, int procedureId);
}
