package ru.gozhan.medicalclinic.persistence.repository;

import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

public interface ProcedureRepository {

    List<ProcedureEntity> findBySpecializationId(int specializationId);

}
