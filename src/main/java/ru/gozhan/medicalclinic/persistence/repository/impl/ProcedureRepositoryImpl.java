package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;
import ru.gozhan.medicalclinic.persistence.repository.ProcedureRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ProcedureRepositoryImpl implements ProcedureRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<ProcedureEntity> findBySpecializationId(int specializationId) {
        log.info("Получение всех процедур по specializations_id={} из таблицы procedures", specializationId);
        String query = "SELECT * FROM procedures WHERE specialization_id = :specializationId";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("specializationId", specializationId);

        return jdbcTemplate.query(query, sqlParameterSource, procedureEntityRowMapper());
    }

    private RowMapper<ProcedureEntity> procedureEntityRowMapper() {
        return (rs, rowNum) -> new ProcedureEntity(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getInt("specialization_id"),
                rs.getInt("cost"),
                rs.getString("description"),
                rs.getInt("duration")
        );
    }

}
