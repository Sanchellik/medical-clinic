package ru.gozhan.medicalclinic.persistence.repository;

import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentDto;
import ru.gozhan.medicalclinic.persistence.entity.AppointmentEntity;
import ru.gozhan.medicalclinic.persistence.entity.PassedAppointmentEntity;

import java.util.List;

public interface AppointmentRepository {
    int save(AppointmentEntity appointmentEntity);

    List<AppointmentDto> findInfoByDoctorId(int doctorId);

    List<AppointmentDto> findInfoByPatientId(int patientId);

    Integer findDoctorIdById(int appointmentId);

    Integer savePassedAppointment(PassedAppointmentEntity passedAppointmentEntity);

    List<PassedAppointmentDto> findPassedInfoByPatientId(int patientId);

    List<PassedAppointmentDto> findPassedInfoByDoctorId(int doctorId);
}
