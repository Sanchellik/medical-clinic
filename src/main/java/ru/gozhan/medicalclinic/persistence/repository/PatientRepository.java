package ru.gozhan.medicalclinic.persistence.repository;

import ru.gozhan.medicalclinic.persistence.entity.PatientEntity;

public interface PatientRepository {
    void update(PatientEntity patientEntity);

    PatientEntity findById(int patientId);

    int save(PatientEntity patientEntity);

    void saveUsersPatients(Long userId, int patientId);
}
