package ru.gozhan.medicalclinic.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.gozhan.medicalclinic.persistence.entity.user.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
