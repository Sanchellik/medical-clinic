package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;
import ru.gozhan.medicalclinic.persistence.repository.DoctorRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class DoctorRepositoryImpl implements DoctorRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<DoctorEntity> findBySpecializationId(int specializationId) {
        log.info("Получение всех врачей по specializations_id={} из таблицы doctors", specializationId);

        String query = """
                SELECT id, name, first_work_day
                FROM doctors
                WHERE specialization_id = :specializationId""";

        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("specializationId", specializationId);

        return jdbcTemplate.query(
                query,
                parameterSource,
                new BeanPropertyRowMapper<>(DoctorEntity.class)
        );
    }

}
