package ru.gozhan.medicalclinic.persistence.repository;

import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

public interface DoctorRepository {

    List<DoctorEntity> findBySpecializationId(int specializationId);

}
