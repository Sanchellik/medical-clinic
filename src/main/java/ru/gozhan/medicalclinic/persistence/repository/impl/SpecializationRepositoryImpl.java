package ru.gozhan.medicalclinic.persistence.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;
import ru.gozhan.medicalclinic.persistence.repository.SpecializationRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class SpecializationRepositoryImpl implements SpecializationRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<SpecializationEntity> findAll() {
        log.info("Получение всех записей из таблицы specializations");
        String selectAllQuery = "SELECT * FROM specializations";

        return jdbcTemplate.query(
                selectAllQuery,
                new BeanPropertyRowMapper<>(SpecializationEntity.class)
        );
    }

}
