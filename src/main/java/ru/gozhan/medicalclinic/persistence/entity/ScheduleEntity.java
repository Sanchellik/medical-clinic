package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Time;
import java.time.DayOfWeek;

@Table("schedules")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleEntity {
    @Id
    private Integer id;
    private int doctorId;
    private DayOfWeek dayOfWeek;
    private Time startTime;
    private Time endTime;
}
