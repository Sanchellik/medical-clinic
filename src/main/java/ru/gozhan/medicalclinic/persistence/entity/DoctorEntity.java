package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Table("doctors")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorEntity {
    @Id
    private Integer id;
    private String name;
    private String phoneNumber;
    private Integer specializationId;
    private LocalDate firstWorkDay;
}
