package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("office")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfficeEntity {
    @Id
    private Integer id;
    private int number;
}
