package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("specializations")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecializationEntity {
    @Id
    private Integer id;
    private String name;
}
