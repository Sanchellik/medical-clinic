package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Table("patients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientEntity {
    @Id
    private Integer id;
    private String name;
    private String phoneNumber;
    private LocalDate birthday;
}
