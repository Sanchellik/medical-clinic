package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Table("appointments")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentEntity {
    @Id
    private Integer id;
    private int patientId;
    private int doctorId;
    private int procedureId;
    private Integer officeId;
    private LocalDateTime dateAndTime;
    private Timestamp createdTimestamp;
}
