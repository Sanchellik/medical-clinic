package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Table("passed_appointments")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassedAppointmentEntity {
    @Id
    private Integer id;
    private int appointmentId;
    private String conclusion;
    private Timestamp timestamp;
}
