package ru.gozhan.medicalclinic.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("procedures")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcedureEntity {
    @Id
    private Integer id;
    private String name;
    private int specializationId;
    private int cost;
    private String description;
    private int duration;
}
