package ru.gozhan.medicalclinic.persistence.entity.user;

public enum Role {
    ROLE_PATIENT, ROLE_REGISTRAR, ROLE_DOCTOR,
}
