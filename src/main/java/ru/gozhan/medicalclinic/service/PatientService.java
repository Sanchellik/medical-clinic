package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.patient.PatientRequest;
import ru.gozhan.medicalclinic.api.dto.patient.PatientResponse;
import ru.gozhan.medicalclinic.api.dto.user.UserDto;

public interface PatientService {
    StatusResponse updatePatient(PatientRequest patientRequest, int userId);

    PatientResponse getPatientByUserId(Long userId);

    void createPatient(UserDto userDto, Long userId);
}
