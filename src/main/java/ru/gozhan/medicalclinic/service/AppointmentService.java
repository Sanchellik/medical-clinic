package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentRequest;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentRequest;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;

import java.util.List;

public interface AppointmentService {

    StatusResponse createAppointment(AppointmentRequest appointmentRequest);

    List<AppointmentDto> getAppointmentsByUserId(Long userId, Role role);

    StatusResponse passAppointment(
            PassedAppointmentRequest passedAppointmentRequest,
            Long userId
    );

    List<PassedAppointmentDto> getPassedAppointmentsByUserId(Long userId, Role role);

}
