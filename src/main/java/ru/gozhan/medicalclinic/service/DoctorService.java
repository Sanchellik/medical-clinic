package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.api.dto.doctor.DoctorBasicInfoDto;

import java.util.List;

public interface DoctorService {

    List<DoctorBasicInfoDto> getBasicInfoBySpecializationId(final int specializationId);

}
