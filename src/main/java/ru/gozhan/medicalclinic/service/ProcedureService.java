package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

public interface ProcedureService {

    List<ProcedureEntity> getBySpecializationId(int specializationId);

}
