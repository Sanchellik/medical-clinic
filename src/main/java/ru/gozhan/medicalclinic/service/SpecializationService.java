package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

public interface SpecializationService {

    List<SpecializationEntity> getAll();

}
