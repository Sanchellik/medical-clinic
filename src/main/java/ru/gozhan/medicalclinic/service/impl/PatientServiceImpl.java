package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.patient.PatientRequest;
import ru.gozhan.medicalclinic.api.dto.patient.PatientResponse;
import ru.gozhan.medicalclinic.api.dto.user.UserDto;
import ru.gozhan.medicalclinic.persistence.entity.PatientEntity;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;
import ru.gozhan.medicalclinic.persistence.repository.PatientRepository;
import ru.gozhan.medicalclinic.service.PatientService;
import ru.gozhan.medicalclinic.service.UserPatientAndDoctorService;

@Service
@RequiredArgsConstructor
@Slf4j
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    private final UserPatientAndDoctorService userPatientAndDoctorService;

    @Override
    public StatusResponse updatePatient(
            PatientRequest patientRequest,
            int userId
    ) {
        int patientId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(userId, Role.ROLE_PATIENT);

        log.info("Обновляем данные пользователя с id = {}, и patientId = {}. Запрос {}", userId, patientId, patientRequest);

        PatientEntity patientEntity = new PatientEntity(
                patientId,
                patientRequest.getName(),
                patientRequest.getPhoneNumber(),
                patientRequest.getBirthday()
        );

        patientRepository.update(patientEntity);

        return new StatusResponse(
                patientId,
                "Данные пациента успешно обновлены"
        );
    }

    @Override
    public PatientResponse getPatientByUserId(Long userId) {
        int patientId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(userId.intValue(), Role.ROLE_PATIENT);

        PatientEntity patientEntity = patientRepository.findById(patientId);

        return new PatientResponse(
                patientId,
                patientEntity.getName(),
                patientEntity.getPhoneNumber(),
                patientEntity.getBirthday()
        );
    }

    @Override
    public void createPatient(UserDto userDto, Long userId) {
        PatientEntity patientEntity = new PatientEntity(
                null,
                userDto.getName(),
                null,
                null
        );

        int patientId = patientRepository.save(patientEntity);

        patientRepository.saveUsersPatients(userId, patientId);
        log.info("Успешная регистрация");
    }

}
