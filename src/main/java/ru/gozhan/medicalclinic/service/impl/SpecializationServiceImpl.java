package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;
import ru.gozhan.medicalclinic.persistence.repository.SpecializationRepository;
import ru.gozhan.medicalclinic.service.SpecializationService;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SpecializationServiceImpl implements SpecializationService {

    private final SpecializationRepository specializationRepository;

    @Override
    public List<SpecializationEntity> getAll() {

        List<SpecializationEntity> specializations =
                specializationRepository.findAll();

        log.info("Найдено {} направлений", specializations.size());

        return specializations;
    }

}
