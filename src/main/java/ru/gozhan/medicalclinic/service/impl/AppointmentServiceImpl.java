package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentRequest;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentRequest;
import ru.gozhan.medicalclinic.persistence.entity.AppointmentEntity;
import ru.gozhan.medicalclinic.persistence.entity.PassedAppointmentEntity;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;
import ru.gozhan.medicalclinic.persistence.repository.AppointmentRepository;
import ru.gozhan.medicalclinic.persistence.repository.OfficeRepository;
import ru.gozhan.medicalclinic.persistence.repository.UserPatientAndDoctorRepository;
import ru.gozhan.medicalclinic.service.AppointmentService;
import ru.gozhan.medicalclinic.service.UserPatientAndDoctorService;
import ru.gozhan.medicalclinic.service.mapper.AppointmentMapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AppointmentServiceImpl implements AppointmentService {

    private final OfficeRepository officeRepository;

    private final AppointmentRepository appointmentRepository;
    private final UserPatientAndDoctorService userPatientAndDoctorService;

    private final AppointmentMapper appointmentMapper;

    @Override
    public StatusResponse createAppointment(
            AppointmentRequest appointmentRequest
    ) {
        log.info("Создание заявки на приём {}", appointmentRequest);

        int patientId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(
                        appointmentRequest.getUserId(),
                        Role.ROLE_PATIENT
                );

        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        int officeId = 1; // TODO fix this mock
//        int officeId = officeRepository
//                .findAvailableByTimestampAndProcedureId(
//                        timestamp,
//                        appointmentRequest.getProcedureId()
//                );

        AppointmentEntity appointmentEntity =
                appointmentMapper.toEntity(appointmentRequest);
        appointmentEntity.setPatientId(patientId);
        appointmentEntity.setOfficeId(officeId);
        appointmentEntity.setCreatedTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        appointmentEntity.setCreatedTimestamp(timestamp);

        int appointmentId = appointmentRepository.save(appointmentEntity);

        return new StatusResponse(
                appointmentId,
                "Заявку успешно создана"
        );
    }

    @Override
    public List<AppointmentDto> getAppointmentsByUserId(Long userId, Role role) {
        int entityId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(userId.intValue(), role);

        List<AppointmentDto> appointments = new ArrayList<>();
        switch (role) {
            case ROLE_DOCTOR -> appointments = appointmentRepository.findInfoByDoctorId(entityId);
            case ROLE_PATIENT -> appointments = appointmentRepository.findInfoByPatientId(entityId);
        }
        return appointments;
    }

    @Override
    public StatusResponse passAppointment(
            PassedAppointmentRequest passedAppointmentRequest,
            Long userId
    ) {
        int doctorId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(userId.intValue(), Role.ROLE_DOCTOR);

        if (doctorId != appointmentRepository
                .findDoctorIdById(
                        passedAppointmentRequest.getAppointmentId()
                )
        ) {
            throw new RuntimeException("Врач хочет отметить проведённым не свой приём");
        }

        int id = appointmentRepository.savePassedAppointment(
                new PassedAppointmentEntity(
                    null,
                    passedAppointmentRequest.getAppointmentId(),
                    passedAppointmentRequest.getConclusion(),
                    Timestamp.valueOf(LocalDateTime.now())
                )
        );

        return new StatusResponse(
                id,
                "Запись о приёме успешно сохранена"
        );
    }

    @Override
    public List<PassedAppointmentDto> getPassedAppointmentsByUserId(
            Long userId, Role role
    ) {
        int entityId = userPatientAndDoctorService
                .getEntityIdByUserIdAndRole(userId.intValue(), role);

        List<PassedAppointmentDto> passedAppointments = new ArrayList<>();
        switch (role) {
            case ROLE_DOCTOR -> passedAppointments = appointmentRepository
                    .findPassedInfoByDoctorId(entityId);

            case ROLE_PATIENT -> passedAppointments = appointmentRepository
                    .findPassedInfoByPatientId(entityId);
        }
        return passedAppointments;
    }

}
