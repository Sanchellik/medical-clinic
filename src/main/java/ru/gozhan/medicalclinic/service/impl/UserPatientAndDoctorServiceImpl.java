package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;
import ru.gozhan.medicalclinic.persistence.repository.UserPatientAndDoctorRepository;
import ru.gozhan.medicalclinic.service.UserPatientAndDoctorService;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserPatientAndDoctorServiceImpl implements UserPatientAndDoctorService {

    private final UserPatientAndDoctorRepository userPatientAndDoctorRepository;

    @Override
    public Integer getEntityIdByUserIdAndRole(int userId, Role role) {
        switch (role) {
            case ROLE_PATIENT -> {
                return userPatientAndDoctorRepository.findPatientIdByUserId(userId);
            }
            case ROLE_DOCTOR -> {
                return userPatientAndDoctorRepository.findDoctorIdByUserId(userId);
            }
        };
        return null;
    }

}
