package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;
import ru.gozhan.medicalclinic.persistence.repository.ProcedureRepository;
import ru.gozhan.medicalclinic.service.ProcedureService;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcedureServiceImpl implements ProcedureService {

    private final ProcedureRepository procedureRepository;

    @Override
    public List<ProcedureEntity> getBySpecializationId(final int specializationId) {

        List<ProcedureEntity> procedures =
                procedureRepository.findBySpecializationId(specializationId);

        log.info("По specializationId={} найдено {} процедуры/процедур",
                specializationId, procedures.size());

        return procedures;
    }

}
