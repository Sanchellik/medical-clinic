package ru.gozhan.medicalclinic.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.api.dto.doctor.DoctorBasicInfoDto;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;
import ru.gozhan.medicalclinic.persistence.repository.DoctorRepository;
import ru.gozhan.medicalclinic.service.DoctorService;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Override
    public List<DoctorBasicInfoDto> getBasicInfoBySpecializationId(
            final int specializationId
    ) {

        final List<DoctorEntity> doctorEntityList =
                doctorRepository.findBySpecializationId(specializationId);
        log.info("По specializationId={} получено {} врача/врачей",
                specializationId, doctorEntityList.size());

        List<DoctorBasicInfoDto> doctorBasicInfoDtoList = new ArrayList<>();

        for (DoctorEntity doctor : doctorEntityList) {
            doctorBasicInfoDtoList.add(
                    new DoctorBasicInfoDto(
                            doctor.getId(),
                            doctor.getName(),
                            calculateYearsOfExperienceByFirstWorkDay(
                                    doctor.getFirstWorkDay()
                            )
                    )
            );
        }
        return doctorBasicInfoDtoList;
    }

    private int calculateYearsOfExperienceByFirstWorkDay(
            final LocalDate firstWorkDay
    ) {
        Period period = firstWorkDay.until(LocalDate.now());
        return period.getYears();
    }

}
