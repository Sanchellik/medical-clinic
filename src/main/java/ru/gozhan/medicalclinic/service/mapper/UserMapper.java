package ru.gozhan.medicalclinic.service.mapper;

import org.mapstruct.Mapper;
import ru.gozhan.medicalclinic.api.dto.user.UserDto;
import ru.gozhan.medicalclinic.persistence.entity.user.User;

@Mapper(componentModel = "spring")
public interface UserMapper extends Mappable<User, UserDto> {
}
