package ru.gozhan.medicalclinic.service.mapper;

import org.mapstruct.Mapper;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentRequest;
import ru.gozhan.medicalclinic.api.dto.user.UserDto;
import ru.gozhan.medicalclinic.persistence.entity.AppointmentEntity;
import ru.gozhan.medicalclinic.persistence.entity.user.User;

@Mapper(componentModel = "spring")
public interface AppointmentMapper
        extends Mappable<AppointmentEntity, AppointmentRequest> {
}
