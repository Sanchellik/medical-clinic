package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.api.dto.auth.JwtRequest;
import ru.gozhan.medicalclinic.api.dto.auth.JwtResponse;

public interface AuthService {

    JwtResponse login(JwtRequest loginRequest);

    JwtResponse refresh(String refreshToken);

}
