package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.persistence.entity.user.User;

public interface UserService {

    User getById(Long id);

    User getByUsername(String username);

    User update(User user);

    User create(User user);

    void delete(Long id);

}

