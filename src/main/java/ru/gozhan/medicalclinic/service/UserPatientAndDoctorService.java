package ru.gozhan.medicalclinic.service;

import ru.gozhan.medicalclinic.persistence.entity.user.Role;

public interface UserPatientAndDoctorService {
    Integer getEntityIdByUserIdAndRole(int userId, Role role);
}
