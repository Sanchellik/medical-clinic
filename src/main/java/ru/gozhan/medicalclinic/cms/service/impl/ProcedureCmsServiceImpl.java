package ru.gozhan.medicalclinic.cms.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.procedure.ProcedureCmsRequest;
import ru.gozhan.medicalclinic.cms.repository.ProcedureCmsRepository;
import ru.gozhan.medicalclinic.cms.service.ProcedureCmsService;
import ru.gozhan.medicalclinic.cms.service.mapper.ProcedureMapper;
import ru.gozhan.medicalclinic.exception.ProcedureNotFoundException;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcedureCmsServiceImpl implements ProcedureCmsService {

    private final ProcedureCmsRepository procedureCmsRepository;
    private final ProcedureMapper procedureMapper;

    @Override
    public List<ProcedureEntity> getAll() {
        return procedureCmsRepository.findAll();
    }

    @Override
    public StatusCmsResponse save(ProcedureCmsRequest procedureCmsRequest) {

        ProcedureEntity procedureEntity =
                procedureMapper.toEntity(procedureCmsRequest);

        return new StatusCmsResponse(
                procedureCmsRepository.save(procedureEntity),
                "Процедура успешно сохранена"
        );
    }

    @Override
    @Transactional
    public StatusCmsResponse delete(int id) {
        if (!procedureCmsRepository.existsById(id)) {
            log.warn("Процедура с id = {} не найдено", id);
            throw new ProcedureNotFoundException("Процедура не найдена");
        }

        procedureCmsRepository.deleteById(id);

        return new StatusCmsResponse(
                id,
                "Процедура успешно удалена"
        );
    }

    @Override
    @Transactional
    public StatusCmsResponse update(
            ProcedureCmsRequest procedureCmsRequest,
            int procedureId
    ) {
        if (!procedureCmsRepository.existsById(procedureId)) {
            log.warn("Процедура с id = {} не найдена", procedureId);
            throw new ProcedureNotFoundException("Процедура не найдена");
        }

        ProcedureEntity procedureEntity =
                procedureMapper.toEntity(procedureCmsRequest);
        procedureEntity.setId(procedureId);

        procedureCmsRepository.update(procedureEntity);

        return new StatusCmsResponse(
                procedureId,
                "Процедура успешно обновлена"
        );
    }

}
