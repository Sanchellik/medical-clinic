package ru.gozhan.medicalclinic.cms.service;

import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.specialization.SpecializationCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

public interface SpecializationCmsService {
    List<SpecializationEntity> getAll();

    StatusCmsResponse save(SpecializationCmsRequest specializationCmsRequest);

    StatusCmsResponse delete(int id);

    StatusCmsResponse update(
            SpecializationCmsRequest specializationCmsRequest,
            int id
    );
}
