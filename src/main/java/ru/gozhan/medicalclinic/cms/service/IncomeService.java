package ru.gozhan.medicalclinic.cms.service;

import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeRequest;
import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeResponse;

public interface IncomeService {
    IncomeResponse calculateIncome(IncomeRequest incomeRequest);
}
