package ru.gozhan.medicalclinic.cms.service;

import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.doctor.DoctorCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

public interface DoctorCmsService {
    List<DoctorEntity> getAll();

    StatusCmsResponse save(DoctorCmsRequest doctorCmsRequest);

    StatusCmsResponse delete(int id);

    StatusCmsResponse update(DoctorCmsRequest doctorCmsRequest, int id);
}
