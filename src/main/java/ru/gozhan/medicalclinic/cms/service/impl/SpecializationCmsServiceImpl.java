package ru.gozhan.medicalclinic.cms.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.specialization.SpecializationCmsRequest;
import ru.gozhan.medicalclinic.cms.repository.SpecializationCmsRepository;
import ru.gozhan.medicalclinic.cms.service.SpecializationCmsService;
import ru.gozhan.medicalclinic.cms.service.mapper.SpecializationMapper;
import ru.gozhan.medicalclinic.exception.SpecializationNotFoundException;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SpecializationCmsServiceImpl implements SpecializationCmsService {

    private final SpecializationCmsRepository specializationCmsRepository;
    private final SpecializationMapper specializationMapper;

    @Override
    public List<SpecializationEntity> getAll() {
        return specializationCmsRepository.findAll();
    }

    @Override
    @Transactional
    public StatusCmsResponse save(SpecializationCmsRequest specializationCmsRequest) {

        SpecializationEntity specializationEntity =
                specializationMapper.toEntity(specializationCmsRequest);

        return new StatusCmsResponse(
                specializationCmsRepository.save(specializationEntity),
                "Направление успешно сохранено"
        );
    }

    @Override
    public StatusCmsResponse delete(int id) {
        if (!specializationCmsRepository.existsById(id)) {
            log.warn("Направление с id = {} не найдено", id);
            throw new SpecializationNotFoundException("Направление не найдено");
        }

        specializationCmsRepository.deleteById(id);

        return new StatusCmsResponse(
                id,
                "Направление успешно удалено"
        );
    }

    @Override
    @Transactional
    public StatusCmsResponse update(
            SpecializationCmsRequest specializationCmsRequest,
            int specializationId
    ) {
        if (!specializationCmsRepository.existsById(specializationId)) {
            log.warn("Направление с id = {} не найдено", specializationId);
            throw new SpecializationNotFoundException("Направление не найдено");
        }

        SpecializationEntity specializationEntity =
                specializationMapper.toEntity(specializationCmsRequest);
        specializationEntity.setId(specializationId);

        specializationCmsRepository.update(specializationEntity);

        return new StatusCmsResponse(
                specializationId,
                "Направление успешно обновлено"
        );
    }

}
