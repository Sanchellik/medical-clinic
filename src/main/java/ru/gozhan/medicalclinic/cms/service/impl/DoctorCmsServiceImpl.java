package ru.gozhan.medicalclinic.cms.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.doctor.DoctorCmsRequest;
import ru.gozhan.medicalclinic.cms.repository.DoctorCmsRepository;
import ru.gozhan.medicalclinic.cms.repository.ScheduleCmsRepository;
import ru.gozhan.medicalclinic.cms.service.DoctorCmsService;
import ru.gozhan.medicalclinic.cms.service.mapper.DoctorMapper;
import ru.gozhan.medicalclinic.exception.DoctorNotFoundException;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DoctorCmsServiceImpl implements DoctorCmsService {

    private final DoctorCmsRepository doctorCmsRepository;
    private final ScheduleCmsRepository scheduleCmsRepository;

    private final DoctorMapper doctorMapper;

    @Override
    public List<DoctorEntity> getAll() {
        return doctorCmsRepository.findAll();
    }

    @Override
    public StatusCmsResponse save(DoctorCmsRequest doctorCmsRequest) {

        DoctorEntity doctorEntity =
                doctorMapper.toEntity(doctorCmsRequest);

        return new StatusCmsResponse(
                doctorCmsRepository.save(doctorEntity),
                "Врач успешно сохранен"
        );
    }

    @Override
    @Transactional
    public StatusCmsResponse delete(int id) {
        if (!doctorCmsRepository.existsById(id)) {
            log.warn("Врач с id = {} не найден", id);
            throw new DoctorNotFoundException("Врач не найден");
        }

        doctorCmsRepository.deleteById(id);

        return new StatusCmsResponse(
                id,
                "Врач успешно удален"
        );
    }

    @Override
    @Transactional
    public StatusCmsResponse update(
            DoctorCmsRequest doctorCmsRequest,
            int doctorId
    ) {
        if (!doctorCmsRepository.existsById(doctorId)) {
            log.warn("Врач с id = {} не найден", doctorId);
            throw new DoctorNotFoundException("Врач не найден");
        }

        DoctorEntity doctorEntity =
                doctorMapper.toEntity(doctorCmsRequest);
        doctorEntity.setId(doctorId);

        doctorCmsRepository.update(doctorEntity);

        return new StatusCmsResponse(
                doctorId,
                "Врач успешно обновлен"
        );
    }

}
