package ru.gozhan.medicalclinic.cms.service.mapper;

import org.mapstruct.Mapper;
import ru.gozhan.medicalclinic.cms.api.dto.specialization.SpecializationCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;
import ru.gozhan.medicalclinic.service.mapper.Mappable;

@Mapper(componentModel = "spring")
public interface SpecializationMapper
        extends Mappable<SpecializationEntity, SpecializationCmsRequest> {
}
