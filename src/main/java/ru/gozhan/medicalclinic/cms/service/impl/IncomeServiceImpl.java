package ru.gozhan.medicalclinic.cms.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeRequest;
import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeResponse;
import ru.gozhan.medicalclinic.cms.api.dto.income.ProcedureIncomeDto;
import ru.gozhan.medicalclinic.cms.repository.IncomeRepository;
import ru.gozhan.medicalclinic.cms.service.IncomeService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class IncomeServiceImpl implements IncomeService {

    private final IncomeRepository incomeRepository;

    @Override
    public IncomeResponse calculateIncome(IncomeRequest incomeRequest) {
        LocalDate fromDate = incomeRequest.getFromDate();
        LocalDate toDate = incomeRequest.getToDate();

        List<ProcedureIncomeDto> proceduresIncome =
                incomeRepository.findIncomeByDates(fromDate, toDate);

        IncomeResponse incomeResponse = new IncomeResponse(
                fromDate,
                toDate,
                proceduresIncome.stream()
                        .map(ProcedureIncomeDto::getCount)
                        .reduce(0, Integer::sum),
                proceduresIncome.stream()
                        .map(ProcedureIncomeDto::getTotalAmount)
                        .reduce(0, Integer::sum),
                proceduresIncome
        );

        return incomeResponse;
    }

}
