package ru.gozhan.medicalclinic.cms.service.mapper;

import org.mapstruct.Mapper;
import ru.gozhan.medicalclinic.cms.api.dto.doctor.DoctorCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;
import ru.gozhan.medicalclinic.service.mapper.Mappable;

@Mapper(componentModel = "spring")
public interface DoctorMapper extends Mappable<DoctorEntity, DoctorCmsRequest> {
}
