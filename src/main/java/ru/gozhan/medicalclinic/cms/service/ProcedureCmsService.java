package ru.gozhan.medicalclinic.cms.service;

import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.procedure.ProcedureCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

public interface ProcedureCmsService {
    List<ProcedureEntity> getAll();

    StatusCmsResponse save(ProcedureCmsRequest procedureCmsRequest);

    StatusCmsResponse delete(int id);

    StatusCmsResponse update(ProcedureCmsRequest procedureCmsRequest, int id);
}
