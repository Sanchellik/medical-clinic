package ru.gozhan.medicalclinic.cms.service.mapper;

import org.mapstruct.Mapper;
import ru.gozhan.medicalclinic.cms.api.dto.procedure.ProcedureCmsRequest;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;
import ru.gozhan.medicalclinic.service.mapper.Mappable;

@Mapper(componentModel = "spring")
public interface ProcedureMapper
        extends Mappable<ProcedureEntity, ProcedureCmsRequest> {
}
