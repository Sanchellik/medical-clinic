package ru.gozhan.medicalclinic.cms.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.cms.repository.ProcedureCmsRepository;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ProcedureCmsRepositoryImpl implements ProcedureCmsRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<ProcedureEntity> findAll() {
        log.info("Получение всех процедур из бд");

        String query = """
                SELECT * FROM procedures ORDER BY id""";

        return jdbcTemplate.query(
                query,
                new BeanPropertyRowMapper<>(ProcedureEntity.class)
        );
    }

    @Override
    public int save(ProcedureEntity procedureEntity) {
        log.info("Сохранение в бд процедуры c именем = {}", procedureEntity.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO procedures (name, specialization_id, cost, description, duration)
                VALUES (:name, :specializationId, :cost, :description, :duration)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", procedureEntity.getName())
                .addValue("specializationId", procedureEntity.getSpecializationId())
                .addValue("cost", procedureEntity.getCost())
                .addValue("description", procedureEntity.getDescription())
                .addValue("duration", procedureEntity.getDuration());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public boolean existsById(int id) {
        String query = """
            SELECT COUNT(*) FROM procedures WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        int count = jdbcTemplate.queryForObject(
                query,
                sqlParameterSource,
                Integer.class
        );

        return count > 0;
    }

    @Override
    public void deleteById(int id) {
        log.info("Удаление процедуры с id = {}", id);

        String query = """
                DELETE FROM procedures
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        jdbcTemplate.update(query, sqlParameterSource);
    }

    @Override
    public void update(ProcedureEntity procedureEntity) {
        log.info("Обновление процедуры. {}", procedureEntity);

        String query = """
                UPDATE procedures
                SET name = :name,
                    specialization_id = :specializationId,
                    cost = :cost,
                    description = :description,
                    duration = :duration
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", procedureEntity.getId())
                .addValue("name", procedureEntity.getName())
                .addValue("specializationId", procedureEntity.getSpecializationId())
                .addValue("cost", procedureEntity.getCost())
                .addValue("description", procedureEntity.getDescription())
                .addValue("duration", procedureEntity.getDuration());

        jdbcTemplate.update(
                query,
                sqlParameterSource
        );
    }
}
