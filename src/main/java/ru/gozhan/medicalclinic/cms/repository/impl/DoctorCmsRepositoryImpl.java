package ru.gozhan.medicalclinic.cms.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.cms.repository.DoctorCmsRepository;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
@Slf4j
public class DoctorCmsRepositoryImpl implements DoctorCmsRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<DoctorEntity> findAll() {
        log.info("Получение всех врачей из бд");

        String query = """
                SELECT * FROM doctors ORDER BY id""";

        return jdbcTemplate.query(
                query,
                new BeanPropertyRowMapper<>(DoctorEntity.class)
        );
    }

    @Override
    public int save(DoctorEntity doctorEntity) {
        log.info("Сохранение в бд врача c именем = {}", doctorEntity.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO doctors (name, "phoneNumber", specialization_id, first_work_day)
                VALUES (:name, :phoneNumber, :specializationId, :firstWorkDay)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", doctorEntity.getName())
                .addValue("phoneNumber", doctorEntity.getPhoneNumber())
                .addValue("specializationId", doctorEntity.getSpecializationId())
                .addValue("firstWorkDay", doctorEntity.getFirstWorkDay());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public boolean existsById(int id) {
        String query = """
            SELECT COUNT(*) FROM doctors WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        int count = jdbcTemplate.queryForObject(
                query,
                sqlParameterSource,
                Integer.class
        );

        return count > 0;
    }

    @Override
    public void deleteById(int id) {
        log.info("Удаление врача с id = {}", id);

        String query = """
                DELETE FROM doctors
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        jdbcTemplate.update(query, sqlParameterSource);
    }

    @Override
    public void update(DoctorEntity doctorEntity) {
        log.info("Обновление врача. {}", doctorEntity);

        String query = """
                UPDATE doctors
                SET name = :name,
                    "phoneNumber" = :phoneNumber,
                    specialization_id = :specializationId,
                    first_work_day = :firstWorkDay
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", doctorEntity.getId())
                .addValue("name", doctorEntity.getName())
                .addValue("phoneNumber", doctorEntity.getPhoneNumber())
                .addValue("specializationId", doctorEntity.getSpecializationId())
                .addValue("firstWorkDay", doctorEntity.getFirstWorkDay());

        jdbcTemplate.update(
                query,
                sqlParameterSource
        );
    }
}
