package ru.gozhan.medicalclinic.cms.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.cms.repository.SpecializationCmsRepository;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
@Slf4j
public class SpecializationCmsRepositoryImpl implements SpecializationCmsRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<SpecializationEntity> findAll() {
        log.info("Получение всех направлений из бд");

        String query = """
                SELECT * FROM specializations ORDER BY id""";

        return jdbcTemplate.query(
                query,
                new BeanPropertyRowMapper<>(SpecializationEntity.class)
        );
    }

    @Override
    public int save(SpecializationEntity specializationEntity) {
        log.info("Сохранение в бд направления = {}", specializationEntity.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        String query = """
                INSERT INTO specializations (name)
                VALUES (:name)""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", specializationEntity.getName());

        jdbcTemplate.update(
                query,
                sqlParameterSource,
                keyHolder,
                new String[]{"id"}
        );

        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public void deleteById(int id) {
        log.info("Удаление направления с id = {}", id);

        String query = """
                DELETE FROM specializations
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        jdbcTemplate.update(query, sqlParameterSource);
    }

    @Override
    public boolean existsById(int id) {
        String query = """
            SELECT COUNT(*) FROM specializations WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        int count = jdbcTemplate.queryForObject(
                query,
                sqlParameterSource,
                Integer.class
        );

        return count > 0;
    }

    @Override
    public void update(SpecializationEntity specializationEntity) {
        log.info("Обновление направления. {}", specializationEntity);

        String query = """
                UPDATE specializations
                SET name = :name
                WHERE id = :id""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", specializationEntity.getName())
                .addValue("id", specializationEntity.getId());

        jdbcTemplate.update(
                query,
                sqlParameterSource
        );
    }

}
