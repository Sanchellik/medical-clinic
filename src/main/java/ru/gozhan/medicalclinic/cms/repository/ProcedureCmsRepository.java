package ru.gozhan.medicalclinic.cms.repository;

import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

public interface ProcedureCmsRepository {
    List<ProcedureEntity> findAll();

    int save(ProcedureEntity procedureEntity);

    boolean existsById(int id);

    void deleteById(int id);

    void update(ProcedureEntity procedureEntity);
}
