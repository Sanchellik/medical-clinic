package ru.gozhan.medicalclinic.cms.repository;

import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

public interface DoctorCmsRepository {
    List<DoctorEntity> findAll();

    int save(DoctorEntity doctorEntity);

    boolean existsById(int id);

    void deleteById(int id);

    void update(DoctorEntity doctorEntity);
}
