package ru.gozhan.medicalclinic.cms.repository;

import ru.gozhan.medicalclinic.cms.api.dto.income.ProcedureIncomeDto;

import java.time.LocalDate;
import java.util.List;

public interface IncomeRepository {
    List<ProcedureIncomeDto> findIncomeByDates(LocalDate fromDate, LocalDate toDate);
}
