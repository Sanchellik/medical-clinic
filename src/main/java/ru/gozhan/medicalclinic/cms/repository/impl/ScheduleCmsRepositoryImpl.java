package ru.gozhan.medicalclinic.cms.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.cms.repository.ScheduleCmsRepository;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ScheduleCmsRepositoryImpl implements ScheduleCmsRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void deleteByDoctorId(int doctorId) {
        log.info("Удаление расписания врача с id = {}", doctorId);

        String query = """
                DELETE FROM schedules
                WHERE doctor_id = :doctorId""";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("doctorId", doctorId);

        jdbcTemplate.update(query, sqlParameterSource);
    }

}
