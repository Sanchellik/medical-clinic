package ru.gozhan.medicalclinic.cms.repository;

import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

public interface SpecializationCmsRepository {
    List<SpecializationEntity> findAll();

    int save(SpecializationEntity specializationEntity);

    void deleteById(int id);

    boolean existsById(int id);

    void update(SpecializationEntity specializationEntity);
}
