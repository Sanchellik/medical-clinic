package ru.gozhan.medicalclinic.cms.repository;

public interface ScheduleCmsRepository {
    void deleteByDoctorId(int doctorId);
}
