package ru.gozhan.medicalclinic.cms.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.gozhan.medicalclinic.cms.api.dto.income.ProcedureIncomeDto;
import ru.gozhan.medicalclinic.cms.repository.IncomeRepository;

import java.time.LocalDate;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class IncomeRepositoryImpl implements IncomeRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<ProcedureIncomeDto> findIncomeByDates(LocalDate fromDate, LocalDate toDate) {
        log.info("Получаем данные о прибыли с {} по {}", fromDate, toDate);

        String query = """
            SELECT p.name,
                   COUNT(*) AS count,
                   SUM(p.cost) AS totalAmount
            FROM appointments a
                JOIN procedures p ON a.procedure_id = p.id
            WHERE a.date_and_time BETWEEN :fromDate AND :toDate
            GROUP BY p.name
            """;

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("fromDate", fromDate)
                .addValue("toDate", toDate);

        return jdbcTemplate.query(
                query,
                sqlParameterSource,
                new BeanPropertyRowMapper<>(ProcedureIncomeDto.class)
        );
    }

}
