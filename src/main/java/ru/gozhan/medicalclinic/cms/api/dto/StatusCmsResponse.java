package ru.gozhan.medicalclinic.cms.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatusCmsResponse {
    private int id;
    private String message;
}
