package ru.gozhan.medicalclinic.cms.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeRequest;
import ru.gozhan.medicalclinic.cms.api.dto.income.IncomeResponse;
import ru.gozhan.medicalclinic.cms.service.IncomeService;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class AdminController {

    private final IncomeService incomeService;

    @GetMapping("income")
    public ResponseEntity<IncomeResponse> getIncomeByDates(
            IncomeRequest incomeRequest
    ) {
        IncomeResponse response = incomeService.calculateIncome(incomeRequest);

        return ResponseEntity.ok(response);
    }

}
