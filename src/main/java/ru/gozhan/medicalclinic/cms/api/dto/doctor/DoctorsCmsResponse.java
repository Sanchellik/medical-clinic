package ru.gozhan.medicalclinic.cms.api.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoctorsCmsResponse {
    private List<DoctorEntity> doctors;
}
