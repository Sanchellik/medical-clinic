package ru.gozhan.medicalclinic.cms.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.procedure.ProcedureCmsRequest;
import ru.gozhan.medicalclinic.cms.api.dto.procedure.ProceduresCmsResponse;
import ru.gozhan.medicalclinic.cms.service.ProcedureCmsService;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cms")
@RequiredArgsConstructor
public class ProcedureCmsController {

    private final ProcedureCmsService procedureCmsService;

    @GetMapping("procedures")
    public ResponseEntity<ProceduresCmsResponse> getAllProcedures() {

        List<ProcedureEntity> procedures =
                procedureCmsService.getAll();

        ProceduresCmsResponse response = ProceduresCmsResponse
                .builder()
                .procedures(procedures)
                .build();

        return ResponseEntity.ok(response);
    }

    @PostMapping("procedures")
    public ResponseEntity<StatusCmsResponse> postProcedure(
            @RequestBody ProcedureCmsRequest procedureCmsRequest
    ) {
        StatusCmsResponse response =
                procedureCmsService.save(procedureCmsRequest);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("procedures/{id}")
    public ResponseEntity<StatusCmsResponse> deleteProcedure(
            @PathVariable int id
    ) {
        StatusCmsResponse response = procedureCmsService.delete(id);

        return ResponseEntity.ok(response);
    }

    @PutMapping("procedures/{id}")
    public ResponseEntity<StatusCmsResponse> putProcedure(
            @PathVariable int id,
            @RequestBody ProcedureCmsRequest procedureCmsRequest
    ) {
        StatusCmsResponse response =
                procedureCmsService.update(procedureCmsRequest, id);

        return ResponseEntity.ok(response);
    }

}
