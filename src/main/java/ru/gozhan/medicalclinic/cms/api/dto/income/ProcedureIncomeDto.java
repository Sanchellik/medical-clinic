package ru.gozhan.medicalclinic.cms.api.dto.income;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcedureIncomeDto {
    private String name;
    private Integer count;
    private Integer totalAmount;
}
