package ru.gozhan.medicalclinic.cms.api.dto.specialization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SpecializationsCmsResponse {
    private List<SpecializationEntity> specializations;
}
