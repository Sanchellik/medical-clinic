package ru.gozhan.medicalclinic.cms.api.dto.procedure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcedureCmsRequest {
    private String name;
    private int specializationId;
    private int cost;
    private String description;
    private int duration;
}
