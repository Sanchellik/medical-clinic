package ru.gozhan.medicalclinic.cms.api.dto.specialization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpecializationCmsRequest {
    private String name;
}
