package ru.gozhan.medicalclinic.cms.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.doctor.DoctorCmsRequest;
import ru.gozhan.medicalclinic.cms.api.dto.doctor.DoctorsCmsResponse;
import ru.gozhan.medicalclinic.cms.service.DoctorCmsService;
import ru.gozhan.medicalclinic.persistence.entity.DoctorEntity;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cms")
@RequiredArgsConstructor
public class DoctorCmsController {

    private final DoctorCmsService doctorCmsService;

    @GetMapping("doctors")
    public ResponseEntity<DoctorsCmsResponse> getAllDoctors() {

        List<DoctorEntity> doctors =
                doctorCmsService.getAll();

        DoctorsCmsResponse response = DoctorsCmsResponse
                .builder()
                .doctors(doctors)
                .build();

        return ResponseEntity.ok(response);
    }

    @PostMapping("doctors")
    public ResponseEntity<StatusCmsResponse> postDoctor(
            @RequestBody DoctorCmsRequest doctorCmsRequest
    ) {
        StatusCmsResponse response =
                doctorCmsService.save(doctorCmsRequest);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("doctors/{id}")
    public ResponseEntity<StatusCmsResponse> deleteDoctor(
            @PathVariable int id
    ) {
        StatusCmsResponse response = doctorCmsService.delete(id);

        return ResponseEntity.ok(response);
    }

    @PutMapping("doctors/{id}")
    public ResponseEntity<StatusCmsResponse> putDoctor(
            @PathVariable int id,
            @RequestBody DoctorCmsRequest doctorCmsRequest
    ) {
        StatusCmsResponse response =
                doctorCmsService.update(doctorCmsRequest, id);

        return ResponseEntity.ok(response);
    }

}
