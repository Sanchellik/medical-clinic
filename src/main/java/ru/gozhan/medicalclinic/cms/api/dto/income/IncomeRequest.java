package ru.gozhan.medicalclinic.cms.api.dto.income;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncomeRequest {
    private LocalDate fromDate;
    private LocalDate toDate;
}
