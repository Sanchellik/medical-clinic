package ru.gozhan.medicalclinic.cms.api.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoctorCmsRequest {
    private String name;
    private String phoneNumber;
    private Integer specializationId;
    private LocalDate firstWorkDay;
}
