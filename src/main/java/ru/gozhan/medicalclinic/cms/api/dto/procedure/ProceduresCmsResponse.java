package ru.gozhan.medicalclinic.cms.api.dto.procedure;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProceduresCmsResponse {
    private List<ProcedureEntity> procedures;
}
