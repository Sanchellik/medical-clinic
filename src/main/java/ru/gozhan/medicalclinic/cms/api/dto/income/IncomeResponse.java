package ru.gozhan.medicalclinic.cms.api.dto.income;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncomeResponse {
    private LocalDate fromDate;
    private LocalDate toDate;
    private Integer count;
    private Integer totalAmount;
    private List<ProcedureIncomeDto> procedures;
}
