package ru.gozhan.medicalclinic.cms.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.cms.api.dto.StatusCmsResponse;
import ru.gozhan.medicalclinic.cms.api.dto.specialization.SpecializationCmsRequest;
import ru.gozhan.medicalclinic.cms.api.dto.specialization.SpecializationsCmsResponse;
import ru.gozhan.medicalclinic.cms.service.SpecializationCmsService;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cms")
@RequiredArgsConstructor
public class SpecializationCmsController {

    private final SpecializationCmsService specializationCmsService;

    @GetMapping("specializations")
    public ResponseEntity<SpecializationsCmsResponse> getAllSpecializations() {

        List<SpecializationEntity> specializations =
                specializationCmsService.getAll();

        SpecializationsCmsResponse response = SpecializationsCmsResponse
                .builder()
                .specializations(specializations)
                .build();

        return ResponseEntity.ok(response);
    }

    @PostMapping("specializations")
    public ResponseEntity<StatusCmsResponse> postSpecialization(
            @RequestBody SpecializationCmsRequest specializationCmsRequest
    ) {
        StatusCmsResponse response =
                specializationCmsService.save(specializationCmsRequest);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("specializations/{id}")
    public ResponseEntity<StatusCmsResponse> deleteSpecialization(
            @PathVariable int id
    ) {
        StatusCmsResponse response = specializationCmsService.delete(id);

        return ResponseEntity.ok(response);
    }

    @PutMapping("specializations/{id}")
    public ResponseEntity<StatusCmsResponse> putSpecialization(
            @PathVariable int id,
            @RequestBody SpecializationCmsRequest specializationCmsRequest
    ) {
        StatusCmsResponse response =
                specializationCmsService.update(specializationCmsRequest, id);

        return ResponseEntity.ok(response);
    }

}
