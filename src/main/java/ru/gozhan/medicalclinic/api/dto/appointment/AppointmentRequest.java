package ru.gozhan.medicalclinic.api.dto.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentRequest {
    private int userId;
    private int doctorId;
    private int procedureId;
    private LocalDateTime dateAndTime;
}
