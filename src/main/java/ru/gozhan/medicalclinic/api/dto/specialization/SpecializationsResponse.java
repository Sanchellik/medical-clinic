package ru.gozhan.medicalclinic.api.dto.specialization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpecializationsResponse {
    private List<SpecializationEntity> specializations;
}
