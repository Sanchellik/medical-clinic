package ru.gozhan.medicalclinic.api.dto.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassedAppointmentRequest {
    private Integer appointmentId;
    private String conclusion;
}
