package ru.gozhan.medicalclinic.api.dto.procedures;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProceduresResponse {
    private List<ProcedureEntity> procedures;
}
