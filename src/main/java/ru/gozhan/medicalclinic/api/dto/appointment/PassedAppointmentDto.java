package ru.gozhan.medicalclinic.api.dto.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassedAppointmentDto {
    private Integer passedAppointmentId;
    private Integer appointmentId;
    private String patientName;
    private String doctorName;
    private String procedureName;
    private String procedureCost;
    private LocalDateTime dateTime;
    private String conclusion;
}
