package ru.gozhan.medicalclinic.api.dto.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(description = "Request for refresh token")
public class JwtRefreshRequest {

    @Schema(description = "refreshToken", example = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJhZG1pbiIsImlkIjoxLCJleHAiOjE3MDA3NzMzMTR9.4bXjZ0164wv5o0wp_gew2BVhw7Iif1K_DL1cp5z0CrLeYPzvMDjIYAXlgdKk-Dry")
    @NotNull(message = "RefreshToken must be not null.")
    private String refreshToken;

}
