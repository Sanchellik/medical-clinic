package ru.gozhan.medicalclinic.api.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorBasicInfoDto {
    private int id;
    private String name;
    private int yearsOfExperience;
}
