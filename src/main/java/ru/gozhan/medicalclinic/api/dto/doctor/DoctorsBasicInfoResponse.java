package ru.gozhan.medicalclinic.api.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoctorsBasicInfoResponse {
    private List<DoctorBasicInfoDto> doctorsBasicInfo;
}
