package ru.gozhan.medicalclinic.api.dto.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentDto {
    private Integer id;
    private String patientName;
    private String doctorName;
    private String procedureName;
    private Integer procedureCost;
    private Integer procedureDuration;
    private Integer officeNumber;
    private LocalDateTime dateTime;
}
