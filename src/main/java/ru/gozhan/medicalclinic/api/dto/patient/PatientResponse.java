package ru.gozhan.medicalclinic.api.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientResponse {
    private Integer id;
    private String name;
    private String phoneNumber;
    private LocalDate birthday;
}
