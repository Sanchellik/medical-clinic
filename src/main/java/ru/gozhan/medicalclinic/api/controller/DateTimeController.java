package ru.gozhan.medicalclinic.api.controller;

import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class DateTimeController {

    @GetMapping(value = "dateTimes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvailableDatesTimes(
            @PathParam("procedureId") final int procedureId,
            @PathParam("doctorId") final int doctorId) {

        return ResponseEntity.ok("""
                {
                    "availableDatesTimes": [
                        {
                            "date": "2023-11-18",
                            "times": [
                                "10:15",
                                "15:30"
                            ]
                        },
                        {
                            "date": "2023-11-23",
                            "times": [
                                "15:30"
                            ]
                        },
                        {
                            "date": "2023-12-17",
                            "times": [
                                "10:15"
                            ]
                        }
                    ]
                }""");
    }

}
