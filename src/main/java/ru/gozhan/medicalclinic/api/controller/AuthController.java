package ru.gozhan.medicalclinic.api.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.auth.JwtRefreshRequest;
import ru.gozhan.medicalclinic.api.dto.auth.JwtRequest;
import ru.gozhan.medicalclinic.api.dto.auth.JwtResponse;
import ru.gozhan.medicalclinic.api.dto.user.UserDto;
import ru.gozhan.medicalclinic.api.dto.validation.OnCreate;
import ru.gozhan.medicalclinic.persistence.entity.user.User;
import ru.gozhan.medicalclinic.service.AuthService;
import ru.gozhan.medicalclinic.service.PatientService;
import ru.gozhan.medicalclinic.service.UserService;
import ru.gozhan.medicalclinic.service.mapper.UserMapper;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Validated
@Tag(name = "Auth Controller", description = "Auth API")
public class AuthController {

    private final AuthService authService;
    private final UserService userService;
    private final PatientService patientService;

    private final UserMapper userMapper;

    @PostMapping("/login")
    public JwtResponse login(@Validated
                             @RequestBody final JwtRequest loginRequest) {
        return authService.login(loginRequest);
    }

    @PostMapping("/register")
    public UserDto register(@Validated(OnCreate.class)
                            @RequestBody final UserDto userDto) {
        User user = userMapper.toEntity(userDto);
        User createdUser = userService.create(user);

        patientService.createPatient(userDto, createdUser.getId());

        return userMapper.toDto(createdUser);
    }

    @PostMapping("/refresh")
    public JwtResponse refresh(@RequestBody final
                                   JwtRefreshRequest refreshRequest) {
        return authService.refresh(refreshRequest.getRefreshToken());
    }

}
