package ru.gozhan.medicalclinic.api.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.AppointmentRequest;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentDto;
import ru.gozhan.medicalclinic.api.dto.appointment.PassedAppointmentRequest;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;
import ru.gozhan.medicalclinic.persistence.entity.user.User;
import ru.gozhan.medicalclinic.security.JwtEntity;
import ru.gozhan.medicalclinic.service.AppointmentService;
import ru.gozhan.medicalclinic.service.UserPatientAndDoctorService;
import ru.gozhan.medicalclinic.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
@Slf4j
public class AppointmentController {

    private final AppointmentService appointmentService;

    private final UserService userService;
    private final UserPatientAndDoctorService userPatientAndDoctorService;

    @PostMapping("appointment")
    public ResponseEntity<StatusResponse> postAppointment(
            @RequestBody AppointmentRequest appointmentRequest
    ) {
        StatusResponse response =
                appointmentService.createAppointment(appointmentRequest);

        return ResponseEntity.ok(response);
    }

    @GetMapping("appointment")
    public ResponseEntity<List<AppointmentDto>> getAppointments() {
        JwtEntity jwtEntity = (JwtEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = userService.getById(jwtEntity.getId());
        Long userId = user.getId();
        Role role = user.getRoles().iterator().next();
        log.info(role.name());

        List<AppointmentDto> response =
                appointmentService.getAppointmentsByUserId(userId, role);

        return ResponseEntity.ok(response);
    }

    @PostMapping("passedAppointments")
    public ResponseEntity<StatusResponse> postPassedAppointment(
            @RequestBody PassedAppointmentRequest request
    ) {
        JwtEntity jwtEntity = (JwtEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = userService.getById(jwtEntity.getId());
        Long userId = user.getId();
        Role role = user.getRoles().iterator().next();

        if (role.equals(Role.ROLE_DOCTOR)) {
            StatusResponse response =
                    appointmentService.passAppointment(request, userId);

            return ResponseEntity.ok(response);
        } else {
            throw new RuntimeException("Запись о приёме должен отмечать врач");
        }
    }

    @GetMapping("passedAppointments")
    public ResponseEntity<List<PassedAppointmentDto>> getPassedAppointments() {
        JwtEntity jwtEntity = (JwtEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = userService.getById(jwtEntity.getId());
        Long userId = user.getId();
        Role role = user.getRoles().iterator().next();
        log.info(role.name());

        List<PassedAppointmentDto> response =
                appointmentService.getPassedAppointmentsByUserId(userId, role);

        return ResponseEntity.ok(response);
    }

}
