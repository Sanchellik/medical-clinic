package ru.gozhan.medicalclinic.api.controller;

import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.doctor.DoctorBasicInfoDto;
import ru.gozhan.medicalclinic.api.dto.doctor.DoctorsBasicInfoResponse;
import ru.gozhan.medicalclinic.service.DoctorService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class DoctorController {

    private final DoctorService doctorService;

    @GetMapping("doctors")
    public ResponseEntity<DoctorsBasicInfoResponse> getDoctorsBySpecializationId(
            @PathParam("specializationId") final int specializationId
    ) {

        List<DoctorBasicInfoDto> doctorBasicInfoDtoList =
                doctorService.getBasicInfoBySpecializationId(specializationId);

        DoctorsBasicInfoResponse response = DoctorsBasicInfoResponse.builder()
                .doctorsBasicInfo(doctorBasicInfoDtoList)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
