package ru.gozhan.medicalclinic.api.controller;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import ru.gozhan.medicalclinic.exception.AccessDeniedException;
import ru.gozhan.medicalclinic.exception.DoctorNotFoundException;
import ru.gozhan.medicalclinic.exception.ExceptionBody;
import ru.gozhan.medicalclinic.exception.ProcedureNotFoundException;
import ru.gozhan.medicalclinic.exception.ResourceNotFoundException;
import ru.gozhan.medicalclinic.exception.SpecializationNotFoundException;

import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.OK)
    public ExceptionBody handleResourceNotFound(
            final ResourceNotFoundException e
    ) {
        return new ExceptionBody(e.getMessage());
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.OK)
    public ExceptionBody handleIllegalState(final IllegalStateException e) {
        return new ExceptionBody(e.getMessage());
    }

    @ExceptionHandler({AccessDeniedException.class,
            org.springframework.security.access.AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ExceptionBody handleAccessDenied() {
        return new ExceptionBody("Access denied.");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.OK)
    public ExceptionBody handleConstraintViolation(
            final ConstraintViolationException e
    ) {
        ExceptionBody exceptionBody = new ExceptionBody("Validation failed.");
        exceptionBody.setErrors(e.getConstraintViolations().stream()
                .collect(Collectors.toMap(
                        violation -> violation.getPropertyPath().toString(),
                        violation -> violation.getMessage()
                )));
        return exceptionBody;
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.OK)
    public ExceptionBody handleAuthentication(final AuthenticationException e) {
        return new ExceptionBody("Authentication failed.");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionBody handleException(final Exception e) {
        e.printStackTrace();
        return new ExceptionBody("Internal error.");
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleMethodArgumentTypeMismatchException(
            final MethodArgumentTypeMismatchException e
    ) {
        ExceptionBody exceptionBody = new ExceptionBody("Wrong pathParam format.");
        exceptionBody.setErrors(Map.of(
                e.getName(),
                e.getParameter().getParameterType().toString()
        ));
        return exceptionBody;
    }

    @ExceptionHandler({
            SpecializationNotFoundException.class,
            ProcedureNotFoundException.class,
            DoctorNotFoundException.class
    })
    @ResponseStatus(HttpStatus.OK)
    public ExceptionBody handleSpecializationNotFoundException(
            final RuntimeException e
    ) {
        return new ExceptionBody(e.getMessage());
    }

}
