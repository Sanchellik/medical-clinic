package ru.gozhan.medicalclinic.api.controller;

import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.procedures.ProceduresResponse;
import ru.gozhan.medicalclinic.persistence.entity.ProcedureEntity;
import ru.gozhan.medicalclinic.service.ProcedureService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ProcedureController {

    private final ProcedureService procedureService;

    @GetMapping("procedures")
    public ResponseEntity<ProceduresResponse> getAllSpecializations(
            @PathParam("specializationId") final int specializationId) {

        List<ProcedureEntity> procedures =
                procedureService.getBySpecializationId(specializationId);

        ProceduresResponse response = ProceduresResponse
                .builder()
                .procedures(procedures)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
