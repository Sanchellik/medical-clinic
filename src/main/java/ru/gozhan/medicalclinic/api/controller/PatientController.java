package ru.gozhan.medicalclinic.api.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.StatusResponse;
import ru.gozhan.medicalclinic.api.dto.patient.PatientRequest;
import ru.gozhan.medicalclinic.api.dto.patient.PatientResponse;
import ru.gozhan.medicalclinic.persistence.entity.user.Role;
import ru.gozhan.medicalclinic.persistence.entity.user.User;
import ru.gozhan.medicalclinic.security.JwtEntity;
import ru.gozhan.medicalclinic.service.PatientService;
import ru.gozhan.medicalclinic.service.UserService;

@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
@Slf4j
public class PatientController {

    private final PatientService patientService;
    private final UserService userService;

    @GetMapping("patients/{id}")
    public ResponseEntity<PatientResponse> getPatient() {
        JwtEntity jwtEntity = (JwtEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = userService.getById(jwtEntity.getId());
        Long userId = user.getId();
        Role role = user.getRoles().iterator().next();

        if (role.equals(Role.ROLE_PATIENT)) {
            PatientResponse response =
                    patientService.getPatientByUserId(userId);

            return ResponseEntity.ok(response);
        } else {
            throw new RuntimeException("Информацию о пациенте хочет получить не пациент");
        }
    }

    @PutMapping("patient")
    public ResponseEntity<StatusResponse> putPatient(
            @RequestBody PatientRequest patientRequest
    ) {
        JwtEntity jwtEntity = (JwtEntity) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        User user = userService.getById(jwtEntity.getId());
        Long userId = user.getId();
        Role role = user.getRoles().iterator().next();

        if (role.equals(Role.ROLE_PATIENT)) {
            StatusResponse response = patientService
                    .updatePatient(patientRequest, userId.intValue());

            return ResponseEntity.ok(response);
        } else {
            throw new RuntimeException("Информацию о пациенте хочет получить не пациент");
        }


    }

}
