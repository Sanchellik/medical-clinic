package ru.gozhan.medicalclinic.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.gozhan.medicalclinic.api.dto.specialization.SpecializationsResponse;
import ru.gozhan.medicalclinic.persistence.entity.SpecializationEntity;
import ru.gozhan.medicalclinic.service.SpecializationService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class SpecializationController {

    private final SpecializationService specializationService;

    @GetMapping("specializations")
    public ResponseEntity<SpecializationsResponse> getAllSpecializations() {

        List<SpecializationEntity> specializations = specializationService.getAll();

        SpecializationsResponse response = SpecializationsResponse
                .builder()
                .specializations(specializations)
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
