package ru.gozhan.medicalclinic.exception;

public class ProcedureNotFoundException extends RuntimeException {

    public ProcedureNotFoundException(String message) {
        super(message);
    }

}
