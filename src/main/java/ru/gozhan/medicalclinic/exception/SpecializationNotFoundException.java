package ru.gozhan.medicalclinic.exception;

public class SpecializationNotFoundException extends RuntimeException {

    public SpecializationNotFoundException(final String message) {
        super(message);
    }

}
